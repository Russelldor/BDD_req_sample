
Administrator section
===============================
 

## Feature: CRUD for slider items

	In order to make website users see new proposals/services,
	as administrator,
	I want to create, update, delete slider items 

### Background:    ###

	Given I am logged as admin 
	And I am on homepage admin page

**Scenario:** Create a new slider item

	When I click on "Add slide"  
	And I upload the jpeg/jpg picture with 1600px/400px resolution  
	And I type in the hyperlink  for RO language
	And I type in the hyperlink  for RU language
	And I choose the publish status to "yes"  
	And I choose the pictures order  
	Then the picture appears on the homepage slider 
	And the creation date of the picture is generated
	
	Example: Slider table

	|Picture       |Hyperlink(RO)		|Hyperlink(RU)      	|Publish status    |Order
	|monkey.jpeg   |unite.md/bang		|unite.md/bangru		|yes			   |1
	|kitten.jpeg   |unite.md/newtarif	|unite.md/newtarifru	|yes			   |2
	|3gBeast.jpg   |unite.md/wow		|unite.md/wowru			|no			   	   |3

**Scenario:** Update slider item  
	
	When I click on "Edit slide"
	And I upload the jpeg/jpg picture with 1600px/400px resolution  
	And I edit in the hyperlink for RO language
	And I edit in the hyperlink for RU language
	And I edit the publish status
	And I edit the pictures order  
	Then the picture appears on the slider  
	And the slider item is updated

**Scenario:** Delete slider item  
	
	When I click on "Delete slide"
	And I confirm that I want to delete it
	Then slider entity should be deleted
	And image file should be deleted from the Front Office


## Feature: CRUD for socials
	
	In order to make my website audience wider,
	As administrator,
	I want to  be able to create and show links to social networks

### Background:    ###

	Given I am logged as admin 
	And I am on homepage admin page


**Scenario:** Create a new social 

	When I press the button "add new"
	And I enter the name of the social
	And I enter the link of the social
	And upload the icon in png format
	And I press the button "create"
	Then the social icon appears at the front office
	
	Example: Social networks table

	|Name 	       |Hyperlink       	|Icon   
	|facebook	   |facebook.com/unite	|fb.png			   
	|twitter	   |twitter.com/unite	|twt.png			   
	|linkedin      |linkedin.com/unite	|li.png			   

**Scenario:** Update socials

	When I press the button "edit"
	And I edit the name of the social
	And I edit the link of the social
	And I edit the icon of the social
	And I press the button "update"
	Then the social item gets updated

**Scenario:** Delete socials

	When I select any social
	And I press "delete" button
	And I confirm that I want to delete it
	The social entity should be deleted

## Feature: CRUD for device properties
	
	In order to show users the list of all device properties
	As administrator,
	I want to create, update and delete them

### Background ###

	Given I am logged as admin 
	And I am on device properties admin page

**Scenario:** Create new device property
	
	When I click on "add new"
	And I enter device property name in RO/RU languages
	And I press "create"
	Then the  device property is created in the Back Office

**Scenario:** Update devices property
	
	When I select devices property
	And I click on "edit"
	And I edit property name in RO/RU languages
	And I press "update"
	Then the property is updated in the Back Office
	And the property is updated in the Front Office

**Scenario:** Delete devices property

	When I activate the  device checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then devices property entity should be deleted
	And devices property file should be deleted from the file system

## Feature: CRUD for badges

	In order to inform my users about special promos, discounts for devices
	As administrator,
	I want to  be able to create, update, delete badges

### Background ###

	Given I am logged as admin 
	And I am on badges admin page

**Scenario:** Create a new badge
	
	When I click on "add new"
	And I enter badge name
	And I upload png image
	And I press "add"
	Then badge is created at the Back Office

**Scenario:** Update a badge

	When I select a badge
	And I click on "edit" button
	And I edit badge name
	And I upload badge image
	And I press "update" button
	Then badge is updated in the Back Office

**Scenario:** Delete a badge
	
	When I select a badge
	And I click on "delete" button
	And I confirm that I want to delete it
	Then badge entity should be deleted
	And badge file should be deleted from Front Office
	
## Feature: CRUD for devices

	In order to inform my users about devices that I sell,
	As administrator,
	I want to  be able to create, update, delete and search devices

### Background ###

	Given I am logged as admin 
	And I am on devices admin page

**Scenario:** Create device

	When I click on "add new"
	And I select device type
	And I enter model
	And I enter private price
	And I enter business price
	And I enter Private description in RO and Ru languages in CK Editor
	And I enter Business description in RO and Ru languages CK Editor
	And I select property
	And I enter property value RO/RU languages
	And I choose the badge
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I activate checkbox "main photo"
	And I activate checkbox "publish"
	And I press "create" button
	Then Mobile device is created in the Back Office
	And the creation date is generated in the Back Ofice
	And the Mobile device is published at the Front Office

	Example: Device table
	
	|Type 	       |Model				|Business Price			|Private Price		|Private descr(RO/RU)	|Business descr(RO/RU)	|Badge		|Property name(RO/RU)		|Photos		       
	|smartphone	   |LG G4 S	   			|200					|250				|CK editor				|CK editor				|promo		|value						|1.jpeg 2.jpeg					
	|tablet		   |Allview AX4 Nano	|1						|110				|CK editor				|CK editor				|top		|value						|1.jpeg 2.jpeg					   
	|modem         |Huawei E 1820		|1						|50					|CK editor				|CK editor				|sale		|value						|1.jpeg 2.jpeg			

	

**Scenario:** Update device

	When I click on "edit" button
	And I edit device type
	And I edit model
	And I edit private price
	And I edit business price
	And I edit Private description in RO and RU languages in CK editor
	And I edit Business description in RO and RU languages in CK editor
	And I edit property value RO/RU languages
	And I add new property
	And I enter property value in RO/RU languages
	And I choose the badge
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I activate checkbox "main photo"
	And I deactivate checkbox "publish"
	And I press "update" button
	Then Mobile device is updated in the Back office
	
**Scenario:** Delete device

	When I activate the device checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then device entity should be deleted
	And device file should be deleted from the file system


**Scenario:** Search device
	
	When I press on "filter" button
	And I select filter properties
	And I enter search value
	Then I should see search results at the Back office

## Feature: Make devices featured

	In order to inform my users about devices that are on sale, promo or discount
	As administrator,
	I want to  be able to flag certain devices as "featured"

### Background ###

	Given I am logged as admin 
	And I am on homepage admin page

**Scenario:** Make the device featured

	When I select the first device model
	When I select the second device model
	When I select the third device model
	When I select the fourth device model
	And I press the button "update"
	Then all selected devices will update at Front Office Homepage
	

## Feature: CRUD for news

	In order to inform my users about some news
	As administrator,
	I want to  be able create, update, delete and search news article

### Background ###

	Given I am logged as admin 
	And I am on news admin page

**Scenario:** Create news article
	
	When I click on add "new article"
	Then CK editor opens
	And I enter title in RO and RU
	And I enter article text in RO and RU
	And I upload image in jpeg/jpg format
	And I activate checkbox status to "publish"
	Then the news article appears at Front Office Homepage
	And the news article appears at Front Office News Page
	And the news article appears at Back Office News page
	And the creation date is generated
	And the Authors name is generated
	
	Example: News Article Table

	|Title(RO/RU)						|Image					|Article text(RO/RU)	|Status			|Creation date		|Authors name	
	|New internet						|internet.jpeg			|CK editor				|published		|09.09.2014			|super.admin
	|All clients will get iPhone 6		|client.jpg				|CK editor				|unpublished	|09.10.2014			|admin.viktor 				
	|Unite bought Vodafone				|unite.jpg				|CK editor				|published		|09.11.2014			|admin.ivan 
	
**Scenario:** Update news article
	
	When I click on "edit article"
	Then CK editor opens
	And I edit title in RO and RU languages
	And I edit article text in RO and RU languages
	And I upload image in jpeg/jpg format
	And I edit creation date
	And I activate chekbox status to "publish"
	And I press "update"
	Then the news article updates at Front Office Homepage
	And the news article updates at Front Office News Page
	And the news article updates at Back Office News page
	And the update date is updated
	And the Authors name is updated

**Scenario:** Delete news article

	When I activate the article checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then article entity should be deleted
	And article file should be deleted from the file system


**Scenario:** Search News article
	
	When I press on "filter" button
	And I select filter properties
	And I enter search value
	Then I should see search results at the Back office

## Feature: CRUD for banners

	In order to inform my users about special occasions, new services
	As administrator,
	I want to  be able create, update, delete banners

### Background ###

	Given I am logged as admin 
	And I am on banners admin page

**Scenario:** Create a banner
	
	When I click on "add new"
	And I select the section from the list
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I enter the hyperlink for RO language
	And I enter the hyperlink for RU language
	And I press the button "create"
	Then the banner appears at the Back Office Banner Page
	And the banner appears at the Front Office chosen Page
	And all subpages of the chosen section has the banner of its parent

	Example: Banners table
	
	|Section			|Photo			|Hyperlink(RO)								|Hyperlink(RU)
	|Promo				|promo.jpeg		|http://ru.unite.md/cartela-3g				|http://ru.unite.md/cartela-3g-ru
	|Prepay				|prepay.jpg		|http://ru.unite.md/servicii_3g_internet	|http://ru.unite.md/servicii_3g_internet-ru
	|Optiuni			|optiuni.jpg	|http://ru.unite.md/optiuni					|http://ru.unite.md/optiuni-ru

**Scenario:** Update a banner
	
	When I click on "edit"
	And I select the page
	And I press "add photo" button
	And I upload the photo in jpeg/jpg format
	And I edit the hyperlink in RO language
	And I edit the hyperlink in RU language
	And I press "update" button
	Then the banner updates at the Back Office Banner Page
	And the banner updates at the Front Office chosen Page
	And all subpages of the chosen section are updated

**Scenario:** Delete banner

	When I activate the banner checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then banner entity should be deleted
	And banner file should be deleted from the Front Office
	

## CRUD for static pages

	In order to have up-to-date content about what we propose, what we do and what we offer
	As administrator,
	I want to  be able create, update, delete static pages

### Background ###

	Given I am logged as admin 
	And I am on static pages admin page

**Scenario:** Create a static page
	
	When I click on "add new"
	And I select page category(menu item)
	And I enter Slug for page
	And I enter name on RO/RU languages
	And I select user type "abonent"
	And I enter content on RO/Ru languages in CK editor
	And I select usert type " non abonent"
	And I enter content on RO/Ru languages in CK editor
	And I press "create page"
	Then the page appears at the Back Office Banner Page
	And the page appears at the Front Office chosen Page
	
	
	Example: Static Page table
	
	|Slug								|Title(RO/RU)						|Content(RO/RU)
	|details-credit-test-1				|credit test						|CK editor
	|new-tariff-unite					|new tariff							|CK editor

**Scenario:** Update static page

	When I click on "edit"
	And I select page category(menu item)
	And I edit Slug for page
	And I edit name on RO/RU languages
	And I edit content on RO/Ru languages in CK editor
	And I press "update page"
	Then the page updates at the Back Office Banner Page
	And the page updates at the Front Office chosen Page

**Scenario:** Delete static page

	When I activate the page checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then page entity should be deleted
	And page file should be deleted from the file system


**Scenario:** Create a block static page
	
	When I click on "add new"
	And I select page category(menu item)
	And I enter Slug for page
	And I enter title in RO/RU languages
	And I enter content on RO/RU languages in CK Editor

	And I press "add new block"
	And I enter block name in RO/RU languages
	And I enter block description in RO/RU languages in CK editor
	And I enter order
	And I upload a picture in jpeg/jpg format

	And I press "add new block"
	And I enter block name in RO/Ru languages
	And I enter block description in RO/Ru languages in CK editor
	And I enter order
	And I upload a picture in jpeg/jpg format
	And I press "create page"

	Then the page appears at the Back Office Banner Page
	And the page appears at the Front Office chosen Page
	
	Example: Block Static Page table
	
	|Slug					|Title(RO/RU)				|Content(RO/RU)			|Block name(RO/RU)			|Block descr(RO/RU)			|Order					|Picture
	|service-list			|service list				|CK editor				|Free internet				|CK editor					|1						|free.jpeg
																				|Free calls every Friday	|CK editor					|2						|calls.jpg
																				|We pay you back			|CK editor					|3						|pay.jpg
																				|Free calls every Friday	|CK editor					|4						|calls.jpg
																				|Free calls every Friday	|CK editor					|5						|calls.jpg

**Scenario:** Edit  a block static page
	
	When I select a page
	And I click on "edit"
	And I select page category (menu item)
	And I edit Slug 
	And I edit name on RO/RU languages
	And I edit content on RO/RU languages

	And I press "add new block"
	And I edit block name in RO/RU languages 
	And I edit block description in RO/RU languages
	And I edit order
	And I upload a picture

	And I press "edit block"
	And I edit block name in RO/Ru languages
	And I edit block description in RO/RU languages
	And I edit order
	And I upload a picture
	And I press "update page"

	Then the page gets updated at the Back Office Banner Page
	And the page gets updated at the Front Office chosen Page
	

**Scenario:** Delete block static page

	When I select the page
	And I press "delete" button 
	And I confirm that I want to delete it
	Then page entity should be deleted
	And page file should be deleted from the file system


## Feature: CRUD shop locations
	
	In order to show users shop list and their geolocation
	As administrator,
	I want to  be able create, update, delete shop position from my back office and to make them visible in the table and google map

### Background ###

	Given I am logged as admin 
	And I am on shop locations admin page

**Scenario:** Create a shop
	
	When I click on "add new"
	And I enter the City name in RO/RU languages
	And I enter the name of the shop in RO/RU languages
	And I enter Address for description in RO/RU languages
	And I enter Schedule 
	And I enter Address for geolocation
	And I Press "set coordinate"
	And I see generated coordinates
	And I see the geolocation on the map
	And I press "create"
	Then the shop appears in the Back Office
	And the shop appers in the Front Office

	Example: Shop list table

	|City(RO/RU)	|Name of the Shop(RO/RU)		|Descr address(RO/RU)			|Geoloc address				|Schedule				|Latitude				|Longitude
	|Tallinn		|Järve Kaubanduskeskus			|Pärnu mnt 234/238				|Pärnu mnt 234/238			|M-F 10:00-23:00			|48.621028				|22.3041488
	|Võru			|Kagukeskus						|Kooli 6						|Kooli mnt 6				|M-F 24h					|21.3041488				|22.3044568
	|Kuressaare		|Ferrumi keskus					|Tallinna mnt 8					|Tallinna mnt 8				|M-F 10:00-21:00			|19.3041488				|16.3041488	

**Scenario:** Edit a shop
	
	When I select a shop
	And I click on "edit"
	And I edit the City name in RO/RU languages
	And I edit the name of the shop in RO/Ru languages
	And I edit Address for description in RO/Ru languages
	And I edit Schedule 
	And I edit Address for geolocation
	And I edit coordinates
	And I press "update"
	Then the shop updates in the Back Office
	And the shop updates in the Front Office

**Scenario:** Delete a shop

	When I activate the page checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then shop entity should be deleted
	And shop file should be deleted from the file system


## Feature: CRUD for operator properties

	In order to show users the list of all operators
	As administrator,
	I want to create, update and delete operators

### Background ###

	Given I am logged as admin 
	And I am on operator properties admin page

**Scenario:** Create operators property
	
	When I click on "add new"
	And I enter operators property name in RO
	And I enter operatoes property name in RU
	And I press "create"
	Then the  operators property is created in the Back Office

	Example: Operators property 
	
	|Property name(RO)					|Property name(RO)
	|Incoming call						|Входящий звонок
	|Outcoming call local				|Исходящий звонок
	|Outcoming call Moldova				|Исходящий звонок в Молдове
	|Outcoming call Internat			|Исходящий международный звонок
	|Outcoming SMS						|Исходящее смс
	|Internet traffic					|Интернет трафик

**Scenario:** Update operators property
	
	When I select operators property
	And I click on "edit"
	And I edit property name in RO
	And I edit property name in RU
	Then the property is created in the Back Office
	And I press "update"
	Then the property is updated in the Back Office
	And the property is updated in the Front Office
	
**Scenario:** Delete operators property

	When I activate the page checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then operators property entity should be deleted
	And operators property file should be deleted from the file system

## Feature: CRUD for zone property 

	In order to show users the list of all zones for international calls
	As administrator,
	I want to create, update and delete zone properties

### Background ###

	Given I am logged as admin 
	And I am on zone property admin page


**Scenario:** Create zone property

	When I click on "add new"
	And I enter zone property name in RO
	And I enter zone property name in RU
	And I select network
	And I press "create"
	Then the zone property is created in the Back Office	
	And the zone property is created in the Front Office
	
	Example: Zone property table

	|Property name(RO)				|Property name(RU)				|Select					
	|rush hour						|В час пик лей/мин.				|Fixed network
	|non/rush hour					|Вне час пик лей/мин.			|Fixed network
	|rush hour						|В час пик лей/мин.				|Mobile network
	|non/rush hour					|В час пик лей/мин.				|Mobile network

	
**Scenario:** Update zone property
	
	When I select zone property
	And I click on "edit"
	And I edit  zone property name in RO
	And I edit zone property name in RU
	And I select network
	Then the zone property is created in the Back Office
	And I press "update"
	Then the zone property is updated in the Back Office
	And the zone property us updated in the Front Office
	
**Scenario:** Delete zone property

	When I activate the page checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then zone property entity should be deleted
	And  zone property file should be deleted from the file system


## Feature: CRUD for zone 

	In order to show users the list of all zones for international calls
	As administrator,
	I want to create, update and delete zones

### Background ###

	Given I am logged as admin 
	And I am on zone admin page

**Scenario:** Create zone

	When I click on "add new"
	And I enter zone name in RO
	And I enter zone name in RU
	And I select several countries from the list
	And I enter numerical values to the properties
	And I click "create"	
	Then the zone is created in the Back Office
	And the zone is created in the Front Office

	Example: Zone table

	|Zone name(RO)			|Zone name(RO)		|Countries			|Fixed rush hour			|Fixed non/rush hour			|Mobile network rush hour			|Mobile non/rush hour
	|Zone 1					|Зона 1				|Ukraine, USA		|1.8						|1.56							|3.6								|2.88
	|2						|2					|Russia, Turkey		|1.8						|1.56							|1.8								|1.56
	|3						|3					|Thailand			|2.4						|2.04							|2.4								|2.04

**Scenario:** Edit zone
	
	When I select a zone
	And I click on "edit"
	And I edit zone name in RO 
	And I edit zone name in RU
	And I edit property values
	And I click "update"	
	Then the zone is updated in the Back Office
	And the zone is updated in the Front Office

**Scenario:** Delete zone

	When I activate zones checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then zone entity should be deleted
	And  zone file should be deleted from the file system

## Feature: CRUD for country 

	In order to show users the list of all countries for international calls
	As administrator,
	I want to create, update and delete zone properties

### Background ###

	Given I am logged as admin 
	And I am on countries admin page

**Scenario:** Create country

	When I click on "add new"
	And I enter country name in RO
	And I enter contry name in RU 
	And I enter country code
	And I select zone from the list
	And I click "create"	
	Then the country is created in the Back Office
	Abd the country is created in the Front Ofice

	Example: Country table
	
	|Country(RO)		|Country(RU)			|Country code						|Zone
	|Transnistria		|Приднестровье			|TS									|1
											
													


**Scenario:** Edit country

	When I select a country
	And I click on "edit"
	And I edit country name in RO
	And I edit country name in RU
	And I edit country code
	And I select zone from the list
	And I click "update"	
	Then the country is updated in the Back Office
	Abd the country is updated in the Front Ofice

**Scenario:** Delete country
	
	When I activate country checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then country entity should be deleted
	And country file should be deleted from the file system

## Feature: CRUD for operators 

	In order to show users the list of all operators
	As administrator,
	I want to create, update and delete operators


### Background ###

	Given I am logged as admin 
	And I am on operators admin page

**Scenario:** Create operator

	When I click "add new"
	And I enter operators name in En language
	And I select country from the list
	And I enter properties value
	And I press "create"
	Then the operator is created in the Back office
	And the country is created in the Front office

	Example: Operator table

	|Operators name(EN)				|Country			|Incoming call			|Outcoming call			|Outcoming call local		|Outcoming call Moldova		|Outcoming call Internat		|Outcoming SMS		|Internet traffic
	|TELE2 Russia					|Russia				|11.2					|17.7					|17.7						|74.8						|1.6							|1.6				|124.7
	|Megafon						|Russia				|11.2					|17.7					|80							|74.8						|9								|9					|345
	|Mobile Telesystems				|Russia				|11.2					|17.7					|36.8						|74.8						|10.4							|10.4				|166.8
					
		
**Scenario:** Update operator

	When I select operator
	And I click on "edit"
	And I edit operators name En language
	And I select country from the list
	And I edit properties value
	And I press "update"
	Then the operator is updated in the Back office
	And the country is updated in the Front office

**Scenario:** Delete operator

	When I activate operator checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then operator entity should be deleted
	And operator file should be deleted from the file system

## Feature: CRUD for admin list  

	In order to manage website
	As administrator,
	I want to create, update and delete admins

### Background ###

	Given I am logged as admin 
	And I am on admins list admin page

**Scenario:** Create admin

	When I click "add new"
	And I enter first name
	And I enter second name
	And I enter username
	And I enter email
	And I enter password
	Then the new admin is created

	Example: Admin list table
	
	|First name				|Second name			|Username			|Email					|Password
	|Ivan					|Ivanov					|admin.ivan			|ivan@unite.md			|123456
	|Viktor					|Tkachenko				|admin.viki			|viki@unite.md			|qwerty
	|Masha					|Hrenova				|admin.masha		|masha@unite.md			|1234qw
	
**Scenario:** Update admin

	When I select admin
	And I press "edit"
	And I edit first name
	And I edit second name
	And I edit username
	And I edit email
	And I edit password
	Then admin data is updated


**Scenario:** Delete admin

	When I activate admin checkbox
	And I press "delete" button 
	And I confirm that I want to delete it
	Then admin entity should be deleted
	And admin file should be deleted from the file system


## Feature: CRUD for coverage area 

	In order to show users our coverage
	As administrator,
	I want to update coverage map

### Background ###

	Given I am logged as admin 
	And I am on coverage admin page

**Scenario:** Update coverage area
	
	When I select coverage area 
	And I upload .kmz file 
	And I press "save"
	And I select 4g coverage area
	And I upload .kmz file 
	And I press "save"
	Then I should see coverage area on Google Maps at the Back Office
	And I should see coverage area on Google Maps at the Front Office

	Example: Coverage area table

	|Coverage name				|KMZ file			|Update date
	|3g							|3g					|10.09.15
	|4g							|4g					|10.08.15


## Feature: CRUD for menu items

	In order to show users proper pages in proper website place
	As administrator,
	I want to create menu items and to assign pages to them

### Background ###

	Given I am logged as admin 
	And I am on admins menu items page

**Scenario:** Create menu item

	When I click on "add"
	And I choose parent category
	And I enter slug
	And I enter category name in RU/RO
	And I select checkbox "Show in menu"
	Then the menu item is created in the Back Office

**Scenario:** Update menu item

	When I select menu item
	And I click on "edit"
	And I edit parent category
	And I edit slug
	And I edit category name in RU/RO
	And I select checkbox "Show in menu"
	And i press "update"
	Then the menu item is updated in the Back Office

**Scenario:** Delete menu item

	When I select menu item
	And I press "delete" button 
	And I confirm that I want to delete it
	Then menu item entity should be deleted
	And menu file should be deleted from the file system

## Feature: Change menu items order

	In order to show users proper menu items
	As administrator,
	I want to change menu items order


### Background ###

	Given I am logged as admin 
	And I am on admins menu editors page

**Scenario:** Change menu items order

	When I select menu items
	And I press up/down
	And I select to show/not to show the page at the Front Office
	Then the menu item change the order with next or previous menu item

## Feature: CRUD for Gallery categories

	In order to manage media files easily
	As administrator,
	I want to create, read, update and delete gallery categories

### Background ###

	Given I am logged as admin 
	And I am on admins menu gallery categories page

**Scenario:** Create a category

	When I click on "add"
	And I enter category name "Jpeg pics"
	And I enter description "Only for jpeg pics"
	And I select file type: img, video, file
	And I select status enabled
	And I press "create"
	Then media category is created

	Example: Media category

	|Category name 		|Description			|File type 		|Status
	|Jpeg pics			|Only for jpeg pics		|img			|enabled

**Scenario:** Update a category

	When I select media category
	And I click on "update"
	And I edit category name
	And I edit file type
	And I edit description
	And I edit status
	And I press "create"
	Then media category gets updated

**Scenario:** Delete a category

	When I select category
	And I click on delete
	And I press OK
	Then category gets updated
	And all media files inside a category switched to parent category

## Feature: CRUD for Gallery files

	In order to show and manage media files
	As administrator,
	I want to upload them and have a nice media structure

### Background ###

	Given I am logged as admin 
	And I am on admins menu gallery page

**Scenario:** Create a media file

	When I click on "add"
	And I choose category "Jpeg pics"
	And I choose file Jedi.jpeg
	And I choose status 
	And I enter description "Jedi img for banners"
	Then the file gets uploaded to the server

	Example: Create a media file
	
	|File name			|Category			|Status				|Description
	|Jedi.jpeg			|Jpeg pics			|Enabled			|Jedi img for banners


**Scenario:** Edit media file
	
	When I click on media file
	And I choose category 
	And I edit name
	And I edit publish status
	And I upload another media file
	And I press "update"
	Then media file gets updated


**Scenario:** Delete media file	
	
	When I select media file
	And I press "delete"
	Then media file should be deleted from the file system
	And it should disappear from the Front Office


**Scenario:** Filter media files

	When I select files name
	And I select files size
	And I select files category
	And I select files status
	Then I should see file with chosen filter parameters
	
	

Front Office section
===============================
 

## Feature: Email form  

	In order to help clients switch to another tariff plan
	As administrator,
	I want users to have easy way to send their wish through contact form

**Scenario:** Send email from contact form

	Given I am on <name_of_the_tariff> page
	When I press the button
	And I enter my name in the required field
	And I enter my phone in the required field
	And I select the city from the dropdown list in the required field
	And I enter google captcha
	Then the operator should get an email

	Example: Email structure

	Name:Ruslan
	Phone:+37369998544
	City: Chisinau
	
	Client would like to switch to <name_of_the_tariff>

## Feature: Search engine 

	In order to help user find what they are looking for
	As administrator,
	I want users to have an option to find content and devices

**Scenario:** Search content success

	Given "Samsung" text in the search field
	When I press "search" the search engine should analyze website content, page names, tags and mobile device name
	Then I should see result page
	And this page should have the name of the page with sought-for content
	And this page should have the page with the device name 

**Scenario:** Search content fail

	Given "Samsung" text in the search field
	When I press "search" the search engine should analyze website content, page names, tags and mobile device name
	Then I should see result page
	And this page should have the next text" Results not found. Please change your search request"


## Feature: Parse and import data from csv file 

	In order to make data input easier,
	As administrator,
	I want to make one time data import from .csv files to my database

**Scenario:** Import Roaming data
	
	Given .csv file with next structure

	|Country Code			|Operator				|Incoming call				|Outcoming call local			|Outcoming call Moldova			|Outcoming Call Internat			|Outcoming SMS			|Internet traffic
	|AT						|Mobilkom Austria AG	|5							|6								|32.2							|75.8								|1						|23	

	When I import this file
	Then the data from the file should be parsed
	And the data should be imported to the existing Database structure
	

**Scenario:** Import International tariff data

	Given .csv file with next structure

	|Zone name(RU)			|Zone name(RO)			|Country Code				|Fixed rush hour			|Fixed non/rush hour			|Mobile network rush hour			|Mobile non/rush hour
	|Зона 1					|Zone 1					|RO, RU, IL					|1.8						|1.56							|3.6								|2.88

	When I import this file
	Then the data from the file should be parsed
	And the data should be imported to the existing Database structure